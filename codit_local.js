/**
 * @file
 * The file is for local javascript that is not theme based.
 *
 * This is the place for any custom Javascript that is NOT specific to the theme
 * or another module. JS placed here would show on ALL themes so theme specific
 * JS should go in the theme itself.
*/  
